import java.io.File

object Main extends App {

  // A MaybePath represents the squares a piece must touch when moving.
  // if the piece is not allowed to move as required, the MaybePath is None
  // The Path must be free for the move to be valid.
  type MaybePath = Option[List[Square]]

  getListOfGames("resources").foreach(fileName => {
    println
    println("################ NEW GAME ################")
    println(s"############## $fileName ##############")
    println
    new Game(s"resources/$fileName")
  })

  def getListOfGames(dir: String): List[String] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).map(f => f.getName).toList
    } else Nil
  }
}

