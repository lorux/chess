import Colors.{Black, Color, White}
import com.whitehatgaming.UserInputFile

class Game(file: String) {

  private val player1 :Player = Player(White)
  private val player2 :Player = Player(Black)
  private val playing :Player = player1

  private val chessBoard = new ChessBoard()

  play(new UserInputFile(file), playing, chessBoard)

  def play(input: UserInputFile, playing: Player, chessBoard: ChessBoard): Unit = {
    chessBoard.asciiPrint()
    println(s"playing $playing")
    printCheckAlerts(chessBoard, playing.color)

    val nextMove = input.nextMove
    if (nextMove != null)
      nextMove.toList match {
        case lineMove: List[Int] =>
          val fromSquare = Square(lineMove.head, lineMove.drop(1).head)
          val toSquare = Square(lineMove.drop(2).head, lineMove.drop(3).head)
          println(s"$playing will move $fromSquare - $toSquare")

          if (canMove(chessBoard, playing, fromSquare, toSquare)) {
            val newBoard = move(chessBoard, fromSquare, toSquare)
              play(input, nextPlayer(playing), newBoard)
          } else play(input, playing, chessBoard)
        case _ =>
      }
  }

  def nextPlayer(player: Player) = if (player == player1) player2 else player1

  def canMove(chessBoard: ChessBoard, playing: Player, from: Square, to: Square): Boolean = {
    errorOnMoving(chessBoard, from, to) match {
      case Some(_) => println
        false
      case None => isPlayerMovingItsPieces(chessBoard, playing, from, to)
    }
  }

  def isPlayerMovingItsPieces(chessBoard: ChessBoard, playing: Player, from: Square, to: Square): Boolean = {
    (chessBoard.get(from), chessBoard.get(to)) match {
      case (Some(pieceOnFrom), _) if pieceOnFrom.color != playing.color =>
        println(s"${errorPremise(from, to)} $playing can only move ${playing.color} pieces")
        false
      case _ => true
    }
  }

  def errorPremise(from: Square, to: Square) : String = s"invalid move ($from -> $to):"

  def errorOnMoving(chessBoard: ChessBoard, from: Square, to: Square): Option[String] = {
    (chessBoard.get(from), chessBoard.get(to)) match {
      case (Some(pieceOnFrom), Some(pieceOnTo)) if pieceOnFrom.color == pieceOnTo.color =>
        Some(s"${errorPremise(from, to)}  destination is occupied by another piece of the same color")
      case (Some(_: Pawn), None) if from.col != to.col =>
        Some(s"${errorPremise(from, to)}  Pawn can move diagonally only when catching")
      case (Some(_: Pawn), Some(_)) if from.col == to.col =>
        Some(s"${errorPremise(from, to)}  Pawn can only catch in diagonal")
      case (Some(pieceOnFrom), _) if chessBoard.isPathFree(pieceOnFrom.getPath(from, to)) => None
      case _ => Some(s"${errorPremise(from, to)}  path must be free")
    }
  }

  def move(chessBoard: ChessBoard, from: Square, to: Square): ChessBoard = {
    chessBoard.get(from).map(pieceOnFrom => new ChessBoard(chessBoard, pieceOnFrom, from, to)).getOrElse(chessBoard)
  }

  def printCheckAlerts(chessBoard: ChessBoard, color: Color): Unit = {
    val opponentKing = chessBoard.state.collect {
      case (s, p) if p.color == color && p.symbol == "K" => s -> p
    }.head
    chessBoard.state.collect {
      case (s, p) if p.color != color && errorOnMoving(chessBoard, s, opponentKing._1).isEmpty => s -> p
    } foreach (v => println(s"Check from ${v._2} to the $color king"))
  }

}

case class Player(color: Color)
