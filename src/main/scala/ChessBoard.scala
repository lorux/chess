import Colors.White
import Colors.Black
import Main.MaybePath

class ChessBoard(val state: Map[Square, Piece]) {

  def this() = this(Map(
    (Square("A", 1), Rook(White)),
    (Square("B", 1), Knight(White)),
    (Square("C", 1), Bishop(White)),
    (Square("D", 1), Queen(White)),
    (Square("E", 1), King(White)),
    (Square("F", 1), Bishop(White)),
    (Square("G", 1), Knight(White)),
    (Square("H", 1), Rook(White)),
    (Square("A", 2), Pawn(White)),
    (Square("B", 2), Pawn(White)),
    (Square("C", 2), Pawn(White)),
    (Square("D", 2), Pawn(White)),
    (Square("E", 2), Pawn(White)),
    (Square("F", 2), Pawn(White)),
    (Square("G", 2), Pawn(White)),
    (Square("H", 2), Pawn(White)),
    (Square("A", 8), Rook(Black)),
    (Square("B", 8), Knight(Black)),
    (Square("C", 8), Bishop(Black)),
    (Square("D", 8), Queen(Black)),
    (Square("E", 8), King(Black)),
    (Square("F", 8), Bishop(Black)),
    (Square("G", 8), Knight(Black)),
    (Square("H", 8), Rook(Black)),
    (Square("A", 7), Pawn(Black)),
    (Square("B", 7), Pawn(Black)),
    (Square("C", 7), Pawn(Black)),
    (Square("D", 7), Pawn(Black)),
    (Square("E", 7), Pawn(Black)),
    (Square("F", 7), Pawn(Black)),
    (Square("G", 7), Pawn(Black)),
    (Square("H", 7), Pawn(Black))
  ))

  def this(old: ChessBoard, piece: Piece, from: Square, to: Square) = {
    this(old.state - from + (to -> piece))
  }

  def get(square: Square): Option[Piece] = state.get(square)

  def isPathFree(path: MaybePath): Boolean = path match {
    case None => false
    case Some(squareList) => !squareList.exists(p => get(p).isDefined)
  }

  def asciiPrint(): Unit = {
    println("------------")
    println("--ABCDEFGH--")
    for (row <- 0 to 7) {
      print(s"${8 - row}>")
      for (col <- 0 to 7)
        print(asciiSquare(Square(col, row)))
      println(s"<${8 - row}")
    }
    println("--ABCDEFGH--")
    println("------------")
  }

  def asciiSquare(square: Square): String = {
    this.get(square) match {
      case None if (square.col + square.row) % 2 == 0 => " "
      case None if (square.col + square.row) % 2 == 1 => "#"
      case Some(piece) if piece.color == White => piece.symbol.toUpperCase()
      case Some(piece) if piece.color == Black => piece.symbol.toLowerCase()
      case _ => "@"
    }
  }
}


case class Square(col: Int, row: Int) {

  override def toString: String = col match {
    case 0 => s"A${8 - row}"
    case 1 => s"B${8 - row}"
    case 2 => s"C${8 - row}"
    case 3 => s"D${8 - row}"
    case 4 => s"E${8 - row}"
    case 5 => s"F${8 - row}"
    case 6 => s"G${8 - row}"
    case 7 => s"H${8 - row}"
  }

  def sameRow(other: Square) : Boolean = this.row == other.row
  def sameCol(other: Square) : Boolean = this.col == other.col
  def sameDiagonal(other:Square) : Boolean = (this.col - other.col).abs == (this.row - other.row).abs
}

object Square {

  def apply(col: String, row: Int): Square = Square(
    col match {
      case "A" => 0
      case "B" => 1
      case "C" => 2
      case "D" => 3
      case "E" => 4
      case "F" => 5
      case "G" => 6
      case "H" => 7
    }, 8 - row
  )

}
