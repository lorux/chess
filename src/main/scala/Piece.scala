import Colors.{Color, White}
import Main.MaybePath

sealed trait Piece {
  val color: Color

  val symbol: String
  lazy val direction: Int = if (color == White) -1 else 1

  def getPath(from: Square, to: Square): MaybePath
}

case class King(color: Color) extends Piece {
  override val symbol: String = "K"

  override def getPath(from: Square, to: Square): MaybePath = {
    if (from == to) None
    else if ((from.col - to.col).abs <= 1 && (from.row - to.row).abs <= 1) Some(Nil)
    else None
  }
}

case class Rook(color: Color) extends Piece {
  override val symbol: String = "R"

  override def getPath(from: Square, to: Square): MaybePath = {
    if (from == to) None
    else if (from.sameRow(to) || from.sameCol(to)) {
      if (from.sameRow(to)) {
        val range = Util.range(from.col, to.col)
        Some(range.map(c => Square(c, from.row)).toList.tail)
      } else {
        val range = Util.range(from.row, to.row)
        Some(range.map(r => Square(from.col, r)).toList.tail)
      }
    } else None
  }
}

case class Bishop(color: Color) extends Piece {
  override val symbol: String = "B"

  override def getPath(from: Square, to: Square): MaybePath = {
    if (from == to) None
    else if (from.sameDiagonal(to)) {
      val colDirection = if (from.col < to.col) +1 else -1
      val rowDirection = if (from.row < to.row) +1 else -1
      Some((0 until (from.col - to.col).abs).map(i => Square(from.col + (i * colDirection), from.row + (i * rowDirection))).toList.tail)
    } else None
  }
}

case class Queen(color: Color) extends Piece {
  override val symbol: String = "Q"

  override def getPath(from: Square, to: Square): MaybePath = {
    if (from == to) None
    else if (from.sameRow(to) || from.sameCol(to)) {
      if (from.sameRow(to)) {
        val range = Util.range(from.col, to.col)
        Some(range.map(c => Square(c, from.row)).toList.tail)
      } else {
        val range = Util.range(from.row, to.row)
        Some(range.map(r => Square(from.col, r)).toList.tail)
      }
    }
    else if (from.sameDiagonal(to)) {
      val colDirection = if (from.col < to.col) 1 else -1
      val rowDirection = if (from.row < to.row) 1 else -1
      Some((0 until (from.col - to.col).abs).map(i => Square(from.col + (i * colDirection), from.row + (i * rowDirection))).toList.tail)
    } else None
  }
}

case class Knight(color: Color) extends Piece {
  override val symbol: String = "N"

  override def getPath(from: Square, to: Square): MaybePath = {
    if (from == to) None
    else {
      val rowMovement = (from.row - to.row).abs
      val colMovement = (from.col - to.col).abs
      if (rowMovement > 0 && colMovement > 0 && rowMovement + colMovement == 3) Some(Nil)
      else None
    }
  }
}


case class Pawn(color: Color) extends Piece {
  override val symbol: String = "P"

  override def getPath(from: Square, to: Square): MaybePath = {
    if (from == to) None
    else if (to.row - from.row == direction && (to.col - from.col).abs <= 1) Some(Nil)
    else if (isStartingSquare(from, direction) && to.row - from.row == 2 * direction && to.col == from.col) Some(List(Square(from.col, from.row + direction)))
    else None
  }

  private def isStartingSquare(square: Square, direction: Int) =
    if (direction == -1) square.row == 6 else square.row == 1
}

object Util {
  def range(first: Int, second: Int): Range = {
    if (first < second) first until second else first until second by -1
  }
}

object Colors extends Enumeration {
  type Color = Value
  val White, Black = Value
}