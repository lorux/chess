import Colors.White, Colors.Black
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.io.ByteArrayOutputStream

class ChessBoardSpec extends AnyWordSpec with Matchers {

  "A ChessBoard with custom setup" should {

    val whiteRook = Rook(White)
    val blackRook = Rook(Black)
    val chessboard = new ChessBoard(Map( (Square("A", 1), whiteRook), (Square("H", 8), blackRook)) )

    "have a white rook in A1" in {
      chessboard.get(Square("A", 1)) shouldBe Some( whiteRook )
    }

    "have a white rook in 0,7" in {
      chessboard.get(Square(0, 7)) shouldBe Some( whiteRook )
    }

    "have a black rook in H8" in {
      chessboard.get(Square("H", 8)) shouldBe Some( blackRook )
    }

    "have none in H7" in {
      chessboard.get(Square("H", 7)) shouldBe None
    }

  }

  "A chessboard" should {
    val chessBoard = new ChessBoard()

    "be printable" in {
      val outputStream = new ByteArrayOutputStream()
      Console.withOut(outputStream) {
        chessBoard.asciiPrint()
      }
      outputStream.toString shouldBe """------------
                                       |--ABCDEFGH--
                                       |8>rnbqkbnr<8
                                       |7>pppppppp<7
                                       |6> # # # #<6
                                       |5># # # # <5
                                       |4> # # # #<4
                                       |3># # # # <3
                                       |2>PPPPPPPP<2
                                       |1>RNBQKBNR<1
                                       |--ABCDEFGH--
                                       |------------
                                       |""".stripMargin
    }
  }

}
