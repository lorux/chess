import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class SquareSpec extends AnyWordSpec with Matchers {

  "a square" should {

    "be unique for the same coordinates" in {
      Square("A",1) shouldBe Square("A", 1)
    }

    "be unique for the chess coordinates and internal coordinates" in {
      Square("A",1) == Square(0, 7) shouldBe true
    }

  }
}
