import Colors.White
import Colors.Black
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec


class PieceSpec extends AnyWordSpec with Matchers {

  "King" should {
    val king = King(White)

    "be able to move by 1" in {
      for(row <- 1 to 3; col <- 1 to 3 if row != col)
      king.getPath(Square(2, 2), Square(col, row)) shouldBe Some(Nil)
    }

    "not be able to stay in the same square" in {
      king.getPath(Square(2, 2), Square(2, 2)) shouldBe None
    }

    "not be able to move by more than 1" in {
      val row = 4
      val col = 4
      for (r <- 0 to 7; c <- 1 to 7 if (r-row).abs > 1 || (c - col).abs > 1)
        king.getPath(Square(col, row), Square(c, r)) shouldBe None
    }
  }

  "Rook" should {

    val rook = Rook(White)

    "be able to move vertically" in {
      rook.getPath(Square(0,0), Square(0,3)) shouldBe Some(List(Square(0,1), Square(0,2)))
    }

    "be able to move horizontally" in {
      rook.getPath(Square(0,0), Square(4,0)) shouldBe Some(List(Square(1,0), Square(2,0), Square(3,0)))
    }

    "not be able to stay imn the same square" in {
      rook.getPath(Square(0,0), Square(0,0)) shouldBe None
    }

    "not be able to move other than horizontally or vertically" in {
      val row = 4
      val col = 4
      for (r <- 0 to 7; c <- 1 to 7 if row != r && c != col)
        rook.getPath(Square(col,row), Square(c,r)) shouldBe None
    }
  }

  "Bishop" should {

    val bishop = Bishop(White)

    "be able to move diagonally in any direction" in {
      bishop.getPath(Square(0,0), Square(3,3)) shouldBe Some(List(Square(1,1), Square(2,2)))
      bishop.getPath(Square(3,4), Square(0,1)) shouldBe Some(List(Square(2,3), Square(1,2)))
      bishop.getPath(Square(0,7), Square(3,4)) shouldBe Some(List(Square(1,6), Square(2,5)))
    }

    "not be able to stay imn the same square" in {
      bishop.getPath(Square(0,0), Square(0,0)) shouldBe None
    }

    "not be able to move other than diagonally" in {
      val row = 4
      val col = 4
      for (r <- 0 to 7; c <- 1 to 7 if (row-r).abs != (c-col).abs)
        bishop.getPath(Square(col,row), Square(c,r)) shouldBe None
    }
  }

  "Queen" should {

    val queen = Queen(White)

    "be able to move diagonally in any direction" in {
      queen.getPath(Square(0,0), Square(3,3)) shouldBe Some(List(Square(1,1), Square(2,2)))
      queen.getPath(Square(3,4), Square(0,1)) shouldBe Some(List(Square(2,3), Square(1,2)))
      queen.getPath(Square(0,7), Square(3,4)) shouldBe Some(List(Square(1,6), Square(2,5)))
    }

    "be able to move vertically" in {
      queen.getPath(Square(0,0), Square(0,3)) shouldBe Some(List(Square(0,1), Square(0,2)))
    }

    "be able to move horizontally" in {
      queen.getPath(Square(0,0), Square(4,0)) shouldBe Some(List(Square(1,0), Square(2,0), Square(3,0)))
    }

    "not be able to stay imn the same square" in {
      queen.getPath(Square(0,0), Square(0,0)) shouldBe None
    }

    "not be able to move other than horizontally or vertically" in {
      val row = 4
      val col = 4
      for (r <- 0 to 7; c <- 1 to 7 if ((row-r).abs != (c-col).abs) && r != row && c != col)
        queen.getPath(Square(col,row), Square(c,r)) shouldBe None
    }
  }

  "Knight" should {

    val knight = Knight(White)

    "be able to do the L move" in {
      val validsquares = List(Square(3,2), Square(5,2),Square(6,3), Square(6,5), Square(5,6), Square(3,6), Square(2,5), Square(2,3))
      for (c <- (0 to 7); r <- (0 to 7) if validsquares.contains(Square(c, r)))
        knight.getPath(Square(4,4), Square(c, r)) shouldBe Some(Nil)
      for (c <- (0 to 7); r <- (0 to 7) if !validsquares.contains(Square(c, r)))
        knight.getPath(Square(4,4), Square(c, r)) shouldBe None
    }

    "not be able to stay imn the same square" in {
      knight.getPath(Square(0,0), Square(0,0)) shouldBe None
    }

  }

  "White Pawn" should {

    val pawn = Pawn(White)

    "be able to do go ahead by 1 in any direction" in { // notice: diagonal-catch controls will be implemented in the chessboard
      val validsquares = List(Square(3,3), Square(4,3),Square(5,3))
      for (c <- (0 to 7); r <- (0 to 7) if validsquares.contains(Square(c, r)))
        pawn.getPath(Square(4,4), Square(c, r)) shouldBe Some(Nil)
      for (c <- (0 to 7); r <- (0 to 7) if !validsquares.contains(Square(c, r)))
        pawn.getPath(Square(4,4), Square(c, r)) shouldBe None
    }

    "be able to do go ahead by 1 in any direction and by 2 if it is in the starting square" in { // notice: diagonal-catch controls will be implemented in the chessboard
      val validSquares = List(Square(3,5), Square(4,5),Square(5,5))
      for (c <- (0 to 7); r <- (0 to 7) if validSquares.contains(Square(c, r)))
        pawn.getPath(Square(4,6), Square(c, r)) shouldBe Some(Nil)

      pawn.getPath(Square(4,6), Square(4, 4)) shouldBe Some(List(Square(4,5)))

      for (c <- (0 to 7); r <- (0 to 7) if !(validSquares :+ Square(4,4)).contains(Square(c, r)))
        pawn.getPath(Square(4,6), Square(c, r)) shouldBe None
    }

    "not be able to stay imn the same square" in {
      pawn.getPath(Square(0,0), Square(0,0)) shouldBe None
    }

  }

  "Black Pawn" should {

    val pawn = Pawn(Black)

    "be able to do go ahead by 1 in any direction" in { // notice: diagonal-catch controls will be implemented in the chessboard
      val validSquares = List(Square(3,5), Square(4,5),Square(5,5))
      val jumpSquare = Square(4, 4)
      for (c <- 0 to 7; r <- 0 to 7 if validSquares.contains(Square(c, r))) {
        pawn.getPath(jumpSquare, Square(c, r)) shouldBe Some(Nil)
      }
      for (c <- 0 to 7; r <- 0 to 7 if !validSquares.contains(Square(c, r)))
        pawn.getPath(jumpSquare, Square(c, r)) shouldBe None
    }

    "be able to do go ahead by 1 in any direction and by 2 if it is in the starting square" in { // notice: diagonal-catch controls will be implemented in the chessboard
      val validSquares = List(Square(3,2), Square(4,2),Square(5,2))
      val jumpSquare = Square(4, 3)
      for (c <- 0 to 7; r <- 0 to 7 if validSquares.contains(Square(c, r)))
        pawn.getPath(Square(4,1), Square(c, r)) shouldBe Some(Nil)

      pawn.getPath(Square(4,1), jumpSquare) shouldBe Some(List(Square(4,2)))

      for (c <- 0 to 7; r <- 0 to 7 if !(validSquares :+ jumpSquare).contains(Square(c, r)))
        pawn.getPath(Square(4,1), Square(c, r)) shouldBe None
    }

    "not be able to stay imn the same square" in {
      pawn.getPath(Square(0,0), Square(0,0)) shouldBe None
    }

  }

}
