#The Chess test

This software reads all the files inside the folder resources and execute them as chess games.

Disclaimer: The files are not validated, so they need to be formally correct.

The files should contain multiple chess moves, each move in a new line.
Each line contains the chess coordinate of the starting and ending squares on a move like: 

*e2e4*

meaning: pick the piece in e2 and put it in e4

the software validates:
- that the player is moving his pieces (withe player can only move white pieces)
- that there is a piece in the starting square
- that the move is valid for the piece in the starting square
- that there are no obstacles in the path
- that the ending square contains an opponent piece, or it is empty
- there is a special handling for the pawns, as they behave differently

Before each move a board is printed as:

```
------------
--ABCDEFGH--
8>rnbqkbnr<8
7>pppppppp<7
6> # # # #<6
5># # # # <5
4> # # # #<4
3># # # # <3
2>PPPPPPPP<2
1>RNBQKBNR<1
--ABCDEFGH--
------------
```

Where you can read the coordinates, the pieces (upper case = white, lower case = black) and, when there is no piece the color of the square ('#' black, ' ' white)'' )  

After the board, the moving player is printed *__Playing Player(White)__*

Then int is printed the move that the player wants to do and, if there is any, the error of the move.

When the move is valid, a new board is printed, and the playing player changes.

When the move is incorrect, an error is printed and the player and the board don't change

